package hangman;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestModel {

	@Test
	void testModel() {
		Model model = new Model("Chocolate");
		assertEquals("CHOCOLATE", model.objective);
		assertEquals("---------", model.current);
		assertEquals("", model.errors);
	}

	@Test
	void testTryLetter() {
		Model model = new Model("Chocolate");
		boolean b = model.tryLetter('o');
		assertTrue(b);
		assertEquals("--O-O----", model.current);
		assertEquals("", model.errors);
		b = model.tryLetter('c');
		assertTrue(b);
		assertEquals("C-OCO----", model.current);
		assertEquals("", model.errors);
		b = model.tryLetter('x');
		assertFalse(b);
		assertEquals("C-OCO----", model.current);
		assertEquals("X", model.errors);
	}

}
